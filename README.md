AVR-Makefile-Suite
==================

Atmell AVR microcontroller's Makedile suite


Usage :
=======

Easy to use and highly configurable Makefile suite
for avr-gcc and AVR assembler. See the example from AVR-Makefile-linux
or go to http://www.absurdme.com for more details.

New in version 2.1 :
====================

This version is created specially for those OSs who use "su" rather than "sudo"
to program the target chip. For example it's tested on Debian-7.2.0. 

